# Mangellan-Dev-Kit

[Magellan-Dev-Kit](http://gitlab.com/horance/magellan-dev-kit) is a development kit for magellan.

Supported Platform:
* [MAC OS X] supported
* [Linux] supported
* [Windows] not supported

Supported Compilers:
* [CLANG] 3.5 or later.
* [GCC] 4.9 or later.
* [MSVC] not supported.

Dependces:
* [Ruby](http://www.ruby-lang.org)
* [CMake](hhtp://www.cmake.org)

## Installation

### Update GCC to 4.9

In order to support full C++11/14 features, to update GCC to 4.9 or later.

    sudo -E add-apt-repository ppa:ubuntu-toolchain-r/test
    sudo apt-get update
    sudo apt-get install gcc-4.9 g++-4.9
    sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-4.9 60 --slave /usr/bin/g++ g++ /usr/bin/g++-4.9
    sudo update-alternatives --config gcc

### Install CMake:

    sudo apt-get install cmake

#### Install RVM:

    curl -L get.rvm.io | bash -s stable
    source ~/.rvm/scripts/rvm
    source ~/.bashrc
    source ~/.bash_profile

#### Install Ruby:

    rvm install 2.1.5

    rvm use 2.1.5
    rvm use 2.1.5 --default
  
### Build Magellan:
  
    rake clone["user"]   # clone magellan from gitlab
    rake build   # build magellan
    rake install # install magellan
    rake unstall # unstall magellan
    rake test    # test magellan
    rake         # default target, test magellan

### Pull/Push Magellan

    rake pull   # pull magellan from gitlab
    rake push["comment for commit"]  # push magellan from gitlab


### Fork/Pull Magellan

You can fork infrastructure, hamcrest, magellan, magellan-dev-kit into private gitlab repositories, then clone them into local workspace.

    rake clone["user"]  # 'user' is your gitlab account.

## Copyright
Copyright (c) 2015-2020 Horance Liu. See LICENSE for details.
