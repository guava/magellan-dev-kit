MODULES = %w[infrastructure hamcrest magellan]

class String
  def red;   "\033[31m#{self}\033[0m" end
  def green; "\033[32m#{self}\033[0m" end
  def blue;  "\033[34m#{self}\033[0m" end
  def bold;  "\033[1m#{self}\033[22m" end
end

def action(task)  
  MODULES.each do |m|
    puts "*".blue*100
    puts "starting #{task} for #{m}".blue.bold
    yield m
  end
end

def exit_on_fail(cmd)
  puts cmd
  system "#{cmd}" or exit
end

task :clone, [:user] do |t, args|
  action(t) do |m|
    system "git clone https://gitlab.com/#{args.user}/#{m}.git"
  end 
end

task :pull do |t|
  action(t) do |m|
    system "cd #{m} && git pull"
  end 
end

task :push, [:comment] do |t, args|
  action(t) do |m|
    system %[cd #{m} && git add -A . && git commit -m"#{args.comment}"]
    system %[cd #{m} && git push origin master]
  end 
end

task :build do |t|
  action(t) do |m|
    system "cd #{m} && mkdir -p build && cd build && cmake .."
    exit_on_fail "cd #{m}/build && make"
  end 
end

task :install => [:build] do |t|
  action(t) do |m|
    system "cd #{m}/build && sudo make install"
  end 
end

task :test => [:install] do |t|
  action(t) do |m|
    exit_on_fail "#{m}/build/test/#{m}-test"
  end 
end

task :unstall do |t|
  action(t) do |m|
    system "sudo rm -rf /usr/local/#{m}"
  end
end 

task :clear do |t|
  action(t) do |m|
    system "rm -rf #{m}/build"
  end 
end

task :default => :test
